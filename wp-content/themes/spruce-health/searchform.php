<div class="search-container">
  <form method="get" class="search-form" id="searchform" action="<?php bloginfo('url'); ?>">
    <input class="search-icon" type="image" height="32" width="32" alt="Search" src="<?php echo get_stylesheet_directory_uri(); ?>/images/search.svg" />
    <input class="search-icon-mobile" type="image" height="32" width="32" alt="Search" src="<?php echo get_stylesheet_directory_uri(); ?>/images/search_white.svg" />
    <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
    <input type="hidden" value="post" name="post_type" id="post_type" />
    <input type="search" placeholder="Search" value="<?php echo get_search_query(); ?>" name="s" id="s" />
  </form>
</div>