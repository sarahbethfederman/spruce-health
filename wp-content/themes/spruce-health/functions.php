<?php
/**
 * Spruce Health
 *
 * This file adds functions to the Genesis Sample Theme.
 *
 * @package Genesis Sample
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    http://www.studiopress.com/
 */

//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Setup Theme
include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

//* Set Localization (do not remove)
load_child_theme_textdomain( 'genesis-sample', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'genesis-sample' ) );

//* Add Image upload and Color select to WordPress Theme Customizer
require_once( get_stylesheet_directory() . '/lib/customize.php' );

//* Include Customizer CSS
include_once( get_stylesheet_directory() . '/lib/output.php' );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Spruce Health' );
define( 'CHILD_THEME_URL', 'http://blog.sprucehealth.com' );
define( 'CHILD_THEME_VERSION', '2.2.4' );

//* Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', 'genesis_sample_enqueue_scripts_styles' );
function genesis_sample_enqueue_scripts_styles() {
	wp_enqueue_style( 'dashicons' );

	wp_enqueue_script( 'genesis-sample-responsive-menu', get_stylesheet_directory_uri() . '/js/responsive-menu.js', array( 'jquery' ), '1.0.0', true );
	$output = array(
		'mainMenu' => __( 'Menu', 'genesis-sample' ),
		'subMenu'  => __( 'Menu', 'genesis-sample' ),
	);
	wp_localize_script( 'genesis-sample-responsive-menu', 'genesisSampleL10n', $output );

}

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );

//* Add Accessibility support
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for custom header
add_theme_support( 'custom-header', array(
	'width'           => 100,
	'height'          => 100,
	'header-selector' => '.title-area',
	'header-text'     => true,
	'flex-height'     => true,
) );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Add support for after entry widget
add_theme_support( 'genesis-after-entry-widget-area' );

//* Add support for 3-column footer widgets
add_theme_support( 'genesis-footer-widgets', 3 );

//* Add Image Sizes
add_image_size( 'featured-image', 720, 400, TRUE );

//* Rename primary and secondary navigation menus
add_theme_support( 'genesis-menus' , array( 'primary' => __( 'After Header Menu', 'genesis-sample' ), 'secondary' => __( 'Footer Menu', 'genesis-sample' ) ) );

//* Reposition the secondary navigation menu
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 5 );

//* Reduce the secondary navigation menu to one level depth
add_filter( 'wp_nav_menu_args', 'genesis_sample_secondary_menu_args' );
function genesis_sample_secondary_menu_args( $args ) {
	if ( 'secondary' != $args['theme_location'] ) {
		return $args;
	}
	$args['depth'] = 1;
	return $args;
}

//* Modify size of the Gravatar in the author box
add_filter( 'genesis_author_box_gravatar_size', 'genesis_sample_author_box_gravatar' );
function genesis_sample_author_box_gravatar( $size ) {
	return 90;
}

//* Modify size of the Gravatar in the entry comments
add_filter( 'genesis_comment_list_args', 'genesis_sample_comments_gravatar' );
function genesis_sample_comments_gravatar( $args ) {
	$args['avatar_size'] = 60;
	return $args;
}

//* SPRUCE EDITS STARTING HERE --------------------------------------

//* Force full-width-content layout setting
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//* Unregister Genesis widgets
add_action( 'widgets_init', 'unregister_genesis_widgets', 20 );
function unregister_genesis_widgets() {
	unregister_widget( 'Genesis_eNews_Updates' );
	unregister_widget( 'Genesis_Featured_Page' );
	unregister_widget( 'Genesis_Featured_Post' );
	unregister_widget( 'Genesis_Latest_Tweets_Widget' );
	unregister_widget( 'Genesis_Menu_Pages_Widget' );
	unregister_widget( 'Genesis_User_Profile_Widget' );
	unregister_widget( 'Genesis_Widget_Menu_Categories' );
}

//* Add new image sizes
add_image_size('grid-thumbnail', 360, 175, TRUE);

//* Delete footer credits
add_filter( 'genesis_footer_creds_text', 'sh_footer_text' );
function sh_footer_text ( $args ) {
  return '';
}

//* Filter the pagination links
add_filter( 'genesis_next_link_text', 'sh_next_link');
function sh_next_link( $args ) {
  return '&gt;';
}

add_filter( 'genesis_prev_link_text', 'sh_prev_link');
function sh_prev_link( $args ) {
  return '&lt;';
}

//* Undo genesis's changes to search form
remove_filter( 'get_search_form', 'genesis_search_form' );

//* Delete the post footers
add_filter( 'genesis_post_meta', 'sh_entry_meta_footer' );
function sh_entry_meta_footer ( $args ) {
  return '';
}

//* Make the logo inline
add_filter( 'genesis_seo_title', 'sh_genesis_replace_header_background_img', 10, 2 );
function sh_genesis_replace_header_background_img( $title, $inside ){
	$inline_logo = sprintf( '<a href="%s" title="%s"><img class="header-logo" src="'. get_stylesheet_directory_uri() .'/images/logo.png" title="%s" alt="%s"/></a>', trailingslashit( home_url() ), esc_attr( get_bloginfo( 'name' ) ), esc_attr( get_bloginfo( 'name' ) ), esc_attr( get_bloginfo( 'name' ) ) );
  $title = $inline_logo . $title;
	return $title;
}

//* Truncate the excerpt
add_filter( 'get_the_excerpt', 'sh_excerpt_length', 999 );
function sh_excerpt_length( $excerpt ) {
  $excerpt = substr($excerpt, 0, 162);
  $excerpt .= "...";
  return $excerpt;
}

//* Edit author info on post pages
add_filter( 'genesis_post_info', 'sh_post_info_filter' );
function sh_post_info_filter($post_info) {
  $author_id = get_the_author_meta('ID');
  $post_info = get_avatar($author_id);
	$post_info .= '[post_author]';
  $post_info .= '<br>Posted on [post_date]';
	return $post_info;
}

//* Add background img on post pages
add_filter( 'genesis_before_entry', 'sh_post_thumbnail' );
function sh_post_thumbnail($post_info) {
  echo '<div class="entry-thumbnail" style="background-image: url(\''. get_the_post_thumbnail_url() .'\');"></div>';
}

//* Add post category to post pages
add_action( 'genesis_entry_header', 'sh_post_category', 1 );
function sh_post_category() {
  echo '<h2 class="entry-category">' . get_the_category()[0]->name . '</h2>';
}

//* Add social media icons to footer
add_action( 'init', 'sh_register_social_menu' );
function sh_register_social_menu() {
  register_nav_menu('footer-social-menu',__( 'Social Media Menu' ));
}
add_filter('genesis_do_subnav', 'sh_social_media_footer');
function sh_social_media_footer($args) {
  echo $args;
  echo wp_nav_menu( array( 'theme_location' => 'footer-social-menu' ));
}

//* Subscribe footer
add_action( 'genesis_before_footer', 'sh_subscribe_footer');
add_action('genesis_header_right', 'do_subscribe_input');
function sh_subscribe_footer() {
  echo '<div class="subscribe-footer">';
  echo '<h3>Get the latest telehealth news and tips delivered to your inbox.</h3>';
  echo '<p>Each monthly newsletter will feature the latest articles…</p>';
  do_subscribe_input();
  echo '</div>';
}

//* Subscribe input
function do_subscribe_input() {
  echo '<div id="mc_embed_signup">';
  echo '<form action="//sprucehealth.us9.list-manage.com/subscribe/post?u=b06109925ca08bd7f35654d7e&amp;id=bd81e87d12" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>';
  echo '<div id="mc_embed_signup_scroll">';
  echo '<input class="subscribe-input" required id="mce-EMAIL" type="email" value="" name="EMAIL" placeholder="Enter your email to get our monthly newsletter">';
  echo '<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_b06109925ca08bd7f35654d7e_bd81e87d12" tabindex="-1" value=""></div>';
  echo '<button class="subscribe-submit" type="submit">Subscribe</button>';
  echo '</div></form></div>';
}

//* Related Posts section
add_action( 'genesis_after_entry', 'sh_related_posts');
function sh_related_posts () {
  $orig_post = $post;
  global $post;
  $categories = get_the_category($post->ID);
  if ($categories) {
    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    $args = array(
      'category__in' => $category_ids,
      'post__not_in' => array($post->ID),
      'posts_per_page'=> 3, // Number of related posts that will be shown.
      'caller_get_posts'=>1
    );
    $my_query = new wp_query( $args );

    if( $my_query->have_posts() ) {
      echo '<div class="related-posts"><h3 class="related-posts-title">Related Articles</h3>';
      while( $my_query->have_posts() ) {
        $my_query->the_post();
        echo '<article class="related-post">';
        echo '<a href="'. get_the_permalink() .'"><div class="related-post-thumbnail" style="background-image: url(\''. get_the_post_thumbnail_url() .'\');"></div></a>';
        echo '<div class="related-post-content">';
        echo '<h2 class="related-post-category">' . get_the_category()[0]->name . '</h2>';
        echo '<h1 class="related-post-title">' . '<a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h1>';
        echo '</div></article>';
      }
      echo '</div>';
    }
  }
  $post = $orig_post;
  wp_reset_query();
}

//* Custom loop for blog, category, search pages
function sh_custom_loop() {
  $counter = 0;
  $ad_args = array(
    'posts_per_page'   => 2,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'post_type'        => 'ad'
  );
  $ads = get_posts($ad_args);

	if ( have_posts() ) :
		while ( have_posts() ) : the_post();
      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

      // first post on first page is featured
      if ($counter == 0 && $paged == 1) {
        echo '<article class="entry featured-entry" style="background-image: url(\''. get_the_post_thumbnail_url() .'\');">';
        echo '<a href="'. get_the_permalink() .'"><div class="entry-thumbnail" style="background-image: url(\''. get_the_post_thumbnail_url() .'\');"></div></a>';
        echo '<div class="entry-content">';
        echo '<div class="entry-content-wrapper">';
        echo '<h2 class="entry-category">' . get_the_category()[0]->name . '</h2>';
        echo '<h1 class="entry-title">' . '<a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h1>';
        echo '<p>' . get_the_excerpt() . '</p>';
        echo '</div></div></article>';
        echo '<article aria-hidden="true" style="display:none"></article>';
      } else {
        if ($counter == 2 && count($ads) >= 1 && $paged == 1) {
          // Show first ad (only on first page)
          $ad = $ads[0];
          $ad_id = $ad->ID;
          ad_post($ad_id);
          $counter++;
        } else if ($counter == 6 && count($ads) >= 2 && $paged == 1) {
          // Show second ad (only on first page)
          $ad = $ads[1];
          $ad_id = $ad->ID;
          ad_post($ad_id);
          $counter++;
        }
        echo '<article class="entry">';
        echo '<a href="'. get_the_permalink() .'"><div class="entry-thumbnail" style="background-image: url(\''. get_the_post_thumbnail_url() .'\');"></div></a>';
        echo '<div class="entry-content">';
        echo '<h2 class="entry-category">' . get_the_category()[0]->name . '</h2>';
        echo '<h1 class="entry-title">' . '<a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h1>';
        echo '<p>' . get_the_excerpt() . '</p>';
        echo '</div></article>';
      }
      $counter++;
		endwhile;
    do_action( 'genesis_after_endwhile' );
  else:
    echo '<h1 class="archive-title">Sorry, no results found.</h1>';
	endif;
	wp_reset_query();
}

//* Custom ads posts
add_action('init', 'sh_ad_post_type');
function sh_ad_post_type() {
  $labels = array(
    'name'               => _x( 'Ads', 'post type general name' ),
    'singular_name'      => _x( 'Ad', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'Ad' ),
    'add_new_item'       => __( 'Add New Ad' ),
    'edit_item'          => __( 'Edit Ad' ),
    'new_item'           => __( 'New Ad' ),
    'all_items'          => __( 'All Ads' ),
    'view_item'          => __( 'View Ad' ),
    'search_items'       => __( 'Search Ads' ),
    'not_found'          => __( 'No Ads found' ),
    'not_found_in_trash' => __( 'No portfolio items found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Ads'
  );

  $args = array(
    'labels'        => $labels,
    'description'   => 'Manages our ads',
    'public'        => true,
    'menu_position' => 5,
    'exclude_from_search' => true,
    'show_in_nav_menus' => false,
    'menu_icon'     => 'dashicons-format-aside',
    'supports'      => array( 'title' )
  );

  register_post_type('ad', $args);
}

//* Ads markup
function ad_post($ad_id) {
  if (get_field('template_type', $ad_id) == "1") {
    if (get_field('background_image', $ad_id)) {
      echo '<article class="entry ad ad-template-1" style="background-image: url(\''. get_field('background_image', $ad_id) .'\');">';
    }
    else {
      echo '<article class="entry ad ad-template-1" style="background-image: url(\''. get_stylesheet_directory_uri() .'/images/ad_bg1.png\');">';
    }
    echo '<div class="entry-content">';
    echo '<img class="ad-logo" src="' . get_stylesheet_directory_uri() .'/images/ad_logo.png">';
    echo '<h1 class="entry-title">' . get_the_title($ad_id) . '</h1>';
    echo '<a class="button" target="_blank" href="' . get_field('button_url', $ad_id) . '">' . get_field('button_label', $ad_id) . '</a>';
    echo '</div></article>';
  } else if (get_field('template_type', $ad_id) == "3") {
    echo '<article class="entry ad ad-template-3">';
    echo '<div class="entry-content">';
    echo '<img class="ad-logo" src="' . get_stylesheet_directory_uri() .'/images/logo.png">';
    echo '<h2 class="entry-category">' . get_field('subtitle', $ad_id) . '</h2>';
    echo '<h1 class="entry-title">' . get_the_title($ad_id) . '</h1>';
    echo '<a class="button" target="_blank" href="' . get_field('button_url', $ad_id) . '">' . get_field('button_label', $ad_id) . '</a>';
    echo '</div></article>';
  } else {
    if (get_field('background_image', $ad_id)) {
      echo '<article class="entry ad ad-template-2" style="background-image: url(\''. get_field('background_image', $ad_id) .'\');">';
    }
    else {
      echo '<article class="entry ad ad-template-2" style="background-image: url(\''. get_stylesheet_directory_uri() .'/images/ad_bg2.png\');">';
    }
    echo '<div class="entry-content">';
    echo '<h2 class="entry-category">' . get_field('subtitle', $ad_id) . '</h2>';
    echo '<h1 class="entry-title">' . get_the_title($ad_id) . '</h1>';
    echo '<a class="button" target="_blank" href="' . get_field('button_url', $ad_id) . '">' . get_field('button_label',$ad_id) . '</a>';
    echo '</div></article>';
  }
}

//* Share links
// Deregister stylesheet Genesis Simple Share
add_action( 'wp_enqueue_scripts', 'sh_deregister_styles' );
function sh_deregister_styles() {
  wp_deregister_style( 'genesis-simple-share-plugin-css' );
}
// Output the share buttons plus the email button
add_action( 'genesis_entry_header', 'sh_share_icons', 0 );
function sh_share_icons() {
  echo '<div class="share-box">';
  echo genesis_share_get_icon_output( 'before_entry_header', array('facebook', 'twitter', 'linkedin') );
  printf( 
    '<div class="email sharrre"><a class="share" href="mailto:?subject=%s&body=%s" target="_blank">tell a friend</a></div>', 
    the_title_attribute( 'echo=0' ),
    sprintf( 'I just read this amazing post I think you would like. Check it out here: %s', get_the_permalink() )
  );
  echo '</div>';
}

//* Mobile menu 
add_action('genesis_do_nav', 'sh_mobile_menu');
function sh_mobile_menu($args) {
  $categories = get_categories( array(
    'parent'  => 0,
    'orderby' => 'count',
    'order'   => 'DESC',
    'number'  => 5,
    'exclude' => 4
  ) );
  echo '<nav class="mobile-menu">';
  get_search_form();
  echo '<h3 class="entry-category">Categories</h3>';
  echo '<ul class="category-menu">';
  foreach ( $categories as $category ) {
    printf( '<li><a href="%1$s">%2$s</a></li>',
        esc_url( get_category_link( $category->term_id ) ),
        esc_html( $category->name )
    );
  }
  echo '</ul>';
  do_subscribe_input();
  genesis_do_subnav(); // about links and social icons
  echo '</nav>';
  return $args;
}
add_action('genesis_header', 'sh_menu_toggle');
function sh_menu_toggle($args) {
  echo '<img class="menu-toggle" src="' . get_stylesheet_directory_uri() . '/images/icon_hamburger.svg">';
  return $args;
}
?>