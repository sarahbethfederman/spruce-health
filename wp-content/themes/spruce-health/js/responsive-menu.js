// New file for Spruce (replacing the genesis sample menu)
( function ( document, $, undefined ) {
  var menuToggle;
  var mobileMenu;
  var siteHeader;
  var isOpen = true;
  var bodyEl;

  $(document).ready(function () {
    menuToggle = $('.menu-toggle');
    mobileMenu = $('.mobile-menu');
    siteHeader = $('.site-header');
    bodyEl = $('body');
    menuToggle.on('click', function() {
      bodyEl.toggleClass('no-scroll');
      isOpen = !isOpen;
      mobileMenu.toggleClass('open');
      siteHeader.toggleClass('menu-open');
      if (isOpen) {
        menuToggle.attr('src', '/wp-content/themes/spruce-health/images/icon_hamburger.svg');
      } else {
        menuToggle.attr('src', '/wp-content/themes/spruce-health/images/icon_close.svg');
      }
    });
  });
})( document, jQuery );