<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Q|2z~L(]@q/uBX.CACvOe1voQgoyUG|~fG{:FF`f#^F-7L;>`QkGn;qgD6G/Fq9F');
define('SECURE_AUTH_KEY',  'VpvF;n~IZ4sfCYE;ijp Y[d11MdORk;2`(S*jM=NA{`s:;gK3&+f%cVuCOc!*Y{$');
define('LOGGED_IN_KEY',    'C/[=k-juW!)1dU@A3?UF:ljuL3C#]m2C]G2EYc3WDL<PB7_On31cfJ&*agL:|K}4');
define('NONCE_KEY',        'F05Sa_<MdO(=HZU|6q;x?fZ$aJh%`#OE9t_=FW1[ jYt<No>E4+;!!2;QMoOA}$v');
define('AUTH_SALT',        'GyLdD+*%dS>K6~&y?NFEvB|$n`V,wmU{f.df9Uo<Qx6w)@XI(cj}{PM0J2#:MfAj');
define('SECURE_AUTH_SALT', '@Dmgx+k`i-^9aE6ZN@TZP?e~W;*0su8ke_O6AQQ`Fy%fF_t}-mCO?4pBT24YH<~n');
define('LOGGED_IN_SALT',   'i[6^iJ6f!n|JK*G=/*sT<BcZru5JPvyAs_o{;c9/Z$)d+X~ReH)K&w(gbju-5a!j');
define('NONCE_SALT',       'H}LF6TKBYHB3d5ws:?Spe<P=!7%o#:,ssd<Y~%s_MAR8gyN%#$UabT`@-OP<1`Pd');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
